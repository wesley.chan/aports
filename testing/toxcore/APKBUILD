# Maintainer: Jonathan Sieber <mail@strfry.org>
pkgname=toxcore
pkgver=0.2.15
pkgrel=0
pkgdesc="Tox communication project - Core Library"
url="https://tox.chat/"
arch="all"
license="GPL-3.0-or-later"
makedepends="linux-headers cmake libsodium-dev opus-dev libvpx-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/TokTok/c-toxcore/archive/v$pkgver.tar.gz"

# Toxcore's tests do require networking and are not working properly in Travis-CI
options="!check"

builddir="$srcdir/c-$pkgname-$pkgver"

build() {
	cmake . \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_LIBDIR=lib

	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	rm -f "$pkgdir"/usr/lib/*.a
}

sha512sums="
4a939e4f4c376cef0d17748abd58b10f3040638be58e5a048e0940cb574ccf96ee353438fbbd321e0c5e9d8bffae6033c057f188d0cde9d410d8cebee2bdf2c4  toxcore-0.2.15.tar.gz
"
