# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=remmina
pkgver=1.4.24
pkgrel=0
pkgdesc="Remmina is a remote desktop client written in GTK+"
url="https://www.remmina.org/"
arch="all" # blocked by libappindicator
license="GPL-2.0-or-later"
makedepends="gtk+3.0-dev intltool zlib-dev libjpeg-turbo-dev gnutls-dev
	vte3-dev libgcrypt-dev libssh-dev libxkbfile-dev freerdp-dev
	avahi-ui-dev libvncserver-dev cmake json-glib-dev libsoup-dev
	openssl1.1-compat-dev libsodium-dev libsecret-dev libappindicator-dev
	pcre2-dev"
replaces="remmina-plugins"
subpackages="$pkgname-dev $pkgname-lang $pkgname-doc"
source="https://gitlab.com/Remmina/Remmina/-/archive/v$pkgver/Remmina-v$pkgver.tar.gz
	remmina-1.0.0-dsofix.patch
	"
options="!check" # No test suite available
builddir="$srcdir"/Remmina-v$pkgver

build() {
	LDFLAGS="$LDFLAGS -lintl" \
	cmake -DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_APPINDICATOR=OFF \
		-DWITH_AVAHI=ON \
		-DWITH_FREERDP=ON \
		-DWITH_GCRYPT=ON \
		-DWITH_LIBSSH=ON \
		-DWITH_PTHREAD=ON \
		-DWITH_TELEPATHY=OFF \
		-DWITH_VTE=ON \
		-DWITH_ZLIB=ON \
		-DWITH_KF5WALLET=ON \
		-DWITH_CUPS=OFF \
		-DWITH_LIBSECRET=ON \
		-DWITH_SPICE=OFF \
		-DWITH_WWW=OFF \
		-DWITH_KF5WALLET=OFF
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
39fecb82cedcd08720f800a69f778125811a141a152e88e8d657f082c5efcc4525483fb8374712db48743f9a0c2222e2312a9b3ecf5f043761d029edda86609d  Remmina-v1.4.24.tar.gz
8c06cfb4cd7eb74f641d6524c2fb5c941022df1d3f428c9f57a88b9714ec602b0baf962a9947aa83de3ccfaed237956743b4eb0083c4dde0cc9740b958bba007  remmina-1.0.0-dsofix.patch
"
