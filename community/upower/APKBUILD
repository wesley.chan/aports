# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=upower
pkgver=0.99.16
pkgrel=0
pkgdesc="Power Management Services"
url="https://upower.freedesktop.org"
# s390x and riscv64 blocked by polkit
arch="all !s390x !riscv64"
license="GPL-2.0-or-later"
makedepends="
	docbook-xsl
	eudev-dev
	glib-dev
	gobject-introspection-dev
	gtk-doc
	libgudev-dev
	libxslt
	meson"
checkdepends="dbus py3-dbus py3-dbusmock py3-gobject3 py3-packaging umockdev-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://gitlab.freedesktop.org/upower/upower/-/archive/v$pkgver/upower-v$pkgver.tar.bz2"
builddir="$srcdir/$pkgname-v$pkgver"

build() {
	abuild-meson \
		-Dudevrulesdir=/lib/udev/rules.d \
		-Dsystemdsystemunitdir=no \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
3efa436e93663c8db57184864157393d48d8b983cefb5df13cd1530e4878f4210468c1bcc0cac08ddedde7e8f4ec3f441206d13aa675ed2c508a05b6aaebd8b8  upower-v0.99.16.tar.bz2
"
