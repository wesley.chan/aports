# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluez-qt
pkgver=5.91.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="Qt wrapper for Bluez 5 DBus API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Multiple tests either hang or fail completely

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="
84982dea512efc3a0e7b8251bc347db221070aa7805d37ba429c7eee301110e87ccc10fff0336143c217e0e67ee37fa47af68119b5d327ef07db884b563ec268  bluez-qt-5.91.0.tar.xz
"
