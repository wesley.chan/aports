# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plymouth-kcm
pkgver=5.24.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://invent.kde.org/plasma/plymouth-kcm"
pkgdesc="KCM to manage the Plymouth (Boot) theme"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcmutils
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	plymouth-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plymouth-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
31079f676bd485402548fb7445df7689e94da73863e09b84f27792a491c18fc9175cb9a0ee49c01db9ae7a5c16ab01b3012a047bde09f8635e55603108418d39  plymouth-kcm-5.24.2.tar.xz
"
