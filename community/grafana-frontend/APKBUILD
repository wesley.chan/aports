# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>

# Make sure to keep this package in sync with grafana.

# Frontend files split from main grafana package.
# This is because grafana's build process is
# > build backend -> test backend -> build frontend -> test frontend
# while alpine doesn't allow easily interleaving those.
# Second problem is grafana doesn't support building on anything except linux/amd64,
# while alpine requires every build to be done natively.
# This is also a reason why we use prebuilt frontend archive.

pkgname=grafana-frontend
pkgver=8.4.1
pkgrel=0
pkgdesc="Open source, feature rich metrics dashboard and graph editor (frontend files)"
url="https://grafana.com"
arch="noarch"
license="AGPL-3.0-only"
options="!check" # We don't build frontend from sources.
source="$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz"
builddir="$srcdir/grafana-$pkgver"

package() {
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/plugins-bundled" "$builddir/public" "$pkgdir/usr/share/grafana/"
}

sha512sums="
802224faaa02872dad114e4ee0b5f8745133eb7010ba292da597e801b85ff869849d9c01ceac66e04e5383b0bfc9e397c9042e0a0dc402f75276e8f2d84671e1  grafana-frontend-8.4.1-bin.tar.gz
"
