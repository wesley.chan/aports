# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Keith Maxwell <keith.maxwell@gmail.com>
pkgname=beancount
pkgver=2.3.5
pkgrel=1
pkgdesc="Double-Entry Accounting from Text Files"
url="https://furius.ca/beancount/"
arch="all !ppc64le"  # limited by py3-grpcio
license="GPL-2.0-only"
depends="python3 py3-dateutil py3-ply py3-bottle py3-lxml py3-magic
	py3-beautifulsoup4 py3-requests py3-chardet py3-pytest
	py3-google-api-python-client"
makedepends="python3-dev py3-setuptools"
source="$pkgname-$pkgver.tar.gz::https://github.com/beancount/beancount/archive/$pkgver.tar.gz"
options="!check"

build() {
	python3 setup.py build
	python3 setup.py build_ext -i
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

check() {
	python3 -m pytest --disable-warnings beancount \
		--deselect=beancount/utils/file_type_test.py::TestFileType::test_xhtml \
		--deselect=beancount/loader_test.py::TestLoadIncludesEncrypted::test_include_encrypted \
		--deselect=beancount/parser/lexer_test.py::TestLexerUnicode::test_bytes_encoded_utf16 \
		--deselect=beancount/ingest/importers/fileonly_test.py::TestFileOnly::test_match
}

sha512sums="
90c075375c53ab34ffc2607236903e96a2fd66b742e1ef76aca90631eca62cd289ece6ab2df2ee42bc208fafefdd9bf9c4b1f858a520d2848414e47594fea772  beancount-2.3.5.tar.gz
"
