# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=evolution-data-server
pkgver=3.42.4
pkgrel=1
pkgdesc="data server for evolution"
options="!check" # Tests fail on the builders, passes CI
url="https://projects.gnome.org/evolution"
arch="all"
license="GPL-2.0-or-later"
depends_dev="libgdata-dev gcr-dev icu-dev"
makedepends="$depends_dev gperf flex bison glib-dev gtk+3.0-dev libsecret-dev
	libsoup-dev libxml2-dev nss-dev sqlite-dev krb5-dev gnu-libiconv-dev
	openldap-dev json-glib-dev webkit2gtk-dev libgweather-dev
	cmake libical-dev libcanberra-dev vala gobject-introspection-dev
	gnome-online-accounts-dev libphonenumber-dev samurai"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/evolution-data-server/${pkgver%.*}/evolution-data-server-$pkgver.tar.xz"

build() {
	CFLAGS="$CFLAGS -I/usr/include/gnu-libiconv" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSYSCONF_INSTALL_DIR=/etc \
		-DCMAKE_BUILD_TYPE=None \
		-DENABLE_GOA=ON \
		-DENABLE_INTROSPECTION=ON \
		-DENABLE_VALA_BINDINGS=ON \
		-DWITH_PHONENUMBER=ON \
		-DWITH_LIBDB=OFF
	cmake --build build
}

check() {
	cd build
	# failing tests on s390x. -locale tests fail due to musl not supporting LC_ADDRESS.
	case "$CARCH" in
		s390x) ;;
		*) CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(test-book-cache-cursor-change-locale|test-sqlite-cursor-change-locale)"
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e2eba6ffeac2924366169cdae4258c7fc7d1172c2aa3a0ced83bea60556627b39ee9ee6f45a64081de232e0c2e801e2cbb19b6fa4a8958ad0f868c87e0b20f4a  evolution-data-server-3.42.4.tar.xz
"
