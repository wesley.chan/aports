# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksshaskpass
pkgver=5.24.2
pkgrel=0
pkgdesc="ssh-add helper that uses kwallet and kpassworddialog"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kwallet
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksshaskpass-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dadc0ed2495545d89bbca4552ac7599d10fffddc528d98d6598f9e53de558c3ea8a63d953192837f31e2d050047cccdf1f900fa7e4db299a5b5c7e22896e6e57  ksshaskpass-5.24.2.tar.xz
"
