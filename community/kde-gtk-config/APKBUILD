# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-gtk-config
pkgver=5.24.2
pkgrel=0
pkgdesc="GTK2 and GTK3 Configurator for KDE"
# armhf blocked by qt5-qtdeclarative
# s390x, riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://invent.kde.org/plasma/kde-gtk-config"
license="GPL-2.0 AND LGPL-2.1-only OR LGPL-3.0-only"
depends="gsettings-desktop-schemas"
makedepends="
	extra-cmake-modules
	gsettings-desktop-schemas-dev
	gtk+2.0-dev
	gtk+3.0-dev
	karchive-dev
	kcmutils-dev
	kconfigwidgets-dev
	kdecoration-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
fac24c617f933cbf4984d7d69e347c67f59cd7cbd3d205ffda0188884fb19eef70260affec956366f2fd7c2dc0508843b5d7171d5575fe4df8d1ceeaeb41c765  kde-gtk-config-5.24.2.tar.xz
"
