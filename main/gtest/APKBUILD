# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=gtest
pkgver=1.11.0
pkgrel=0
pkgdesc="Google Test - C++ testing utility based on the xUnit framework (like JUnit)"
url="https://github.com/google/googletest"
arch="all"
license="BSD-3-Clause"
depends_dev="$pkgname=$pkgver-r$pkgrel gmock=$pkgver-r$pkgrel cmake"
makedepends="$depends_dev python3-dev samurai"
subpackages="$pkgname-dev gmock"
source="$pkgname-$pkgver.tar.gz::https://github.com/google/googletest/archive/$_githash.tar.gz"

builddir="$srcdir/googletest-release-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DPYTHON_EXECUTABLE=python3 \
		-Dgtest_build_tests=ON \
		-DBUILD_GMOCK=ON
	cmake --build build
}

check() {
	# fails of x86
	ctest --test-dir build -E "googletest-port-test"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
	find "$pkgdir" -name '*.pump' -print -delete
}

gmock() {
	pkgdesc="Google Mock - A library for writing and using C++ mock classes"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgmock*.so* "$subpkgdir"/usr/lib/
}

sha512sums="
6fcc7827e4c4d95e3ae643dd65e6c4fc0e3d04e1778b84f6e06e390410fe3d18026c131d828d949d2f20dde6327d30ecee24dcd3ef919e21c91e010d149f3a28  gtest-1.11.0.tar.gz
"
